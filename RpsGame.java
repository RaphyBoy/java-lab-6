import java.util.Random;

public class RpsGame{
    private int ties = 0;
    private int wins = 0;
    private int losses = 0;
    private Random rand = new Random();

    public int getLosses() {
        return losses;
    }
    public int getTies() {
        return ties;
    }
    public int getWins() {
        return wins;
    }

    public String PlayRound(String Pchoice){
        //0 = rock, 1 = paper, 2 = scissors
        int AIchoiceINT = rand.nextInt(3);
        String AIchoice;
        if (AIchoiceINT == 0){
            AIchoice = "Rock";
        }else if(AIchoiceINT == 1){
            AIchoice = "Paper";
        }else{
            AIchoice = "Scissors";
        }

        String returnString = "";

        if(AIchoice.equals(Pchoice)){
            returnString = "You both played " + AIchoice + " and Tied";
            ties++;
        }else if(Pchoice.equals("Rock") && AIchoice.equals("Paper")){
            returnString = "Computer played " + AIchoice + " and Won";
            losses++;
        }else if(Pchoice.equals("Rock") && AIchoice.equals("Scissors")){
            returnString = "Computer played " + AIchoice + " and Lost";
            wins++;
        }else if(Pchoice.equals("Paper") && AIchoice.equals("Rock")){
            returnString = "Computer played " + AIchoice + " and Lost";
            wins++;
        }else if(Pchoice.equals("Paper") && AIchoice.equals("Scissors")){
            returnString = "Computer played " + AIchoice + " and Won";
            losses++;
        }else if(Pchoice.equals("Scissors") && AIchoice.equals("Paper")){
            returnString = "Computer played " + AIchoice + " and Lost";
            wins++;
        }else if(Pchoice.equals("Scissors") && AIchoice.equals("Rock")){
            returnString = "Computer played " + AIchoice + " and Won";
            losses++;
        }

        return returnString;
    }
}