import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class RpsApplication extends Application {
    private RpsGame game = new RpsGame();
    @Override
    public void start(Stage stage) {
		Group root = new Group(); 

        //scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

        HBox hb1 = new HBox();
        HBox hb2 = new HBox();

        VBox vb1 = new VBox();

        Button Brock = new Button("Rock");
        Button Bscissors = new Button("Scissors");
        Button Bpaper = new Button("Paper");
        TextField TFMessage = new TextField("Welcome!");
        TFMessage.setPrefWidth(300);
        TextField TFWins = new TextField("Wins: " + game.getWins());
        TFWins.setPrefWidth(75);
        TextField TFLosses = new TextField("Losses: " + game.getLosses());
        TFLosses.setPrefWidth(75);
        TextField TFTies = new TextField("Ties: " + game.getTies());
        TFTies.setPrefWidth(75);

        hb1.getChildren().addAll(Brock,Bscissors,Bpaper);
        hb2.getChildren().addAll(TFMessage,TFLosses,TFTies,TFWins);

        vb1.getChildren().addAll(hb1,hb2);

        root.getChildren().addAll(vb1);
		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show();
        
        Brock.setOnAction(new RpsChoice(TFWins, TFLosses, TFTies, TFMessage, game, "Rock"));
        Bpaper.setOnAction(new RpsChoice(TFWins, TFLosses, TFTies, TFMessage, game, "Paper"));
        Bscissors.setOnAction(new RpsChoice(TFWins, TFLosses, TFTies, TFMessage, game, "Scissors"));

	}

    public static void main(String[] args) {
        launch();
    }
}
