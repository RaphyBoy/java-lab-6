import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent>{
    private TextField TFWins;
    private TextField TFLosses;
    private TextField TFTies;
    private TextField TFMessage;
    private RpsGame game;
    private String Pchoice;

    public RpsChoice(TextField TFWins,TextField TFLosses,TextField TFTies,TextField TFMessage,RpsGame game,String Pchoice){
        this.Pchoice = Pchoice;
        this.TFLosses = TFLosses;
        this.TFWins = TFWins;
        this.TFTies = TFTies;
        this.TFMessage = TFMessage;
        this.game = game;
    }
    
    @Override
    public void handle(ActionEvent e) {
        String message = this.game.PlayRound(this.Pchoice);
        this.TFMessage.setText(message);
        this.TFWins.setText("Wins: " + this.game.getWins());
        this.TFLosses.setText("Losses: " + this.game.getLosses());
        this.TFTies.setText("Ties: " + this.game.getTies());
    }
    
}
